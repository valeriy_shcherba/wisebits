#include "NativePlugin.h"
#include <math.h>

void GenerateAuraTexture(void* pixels, int textureSize, int time)
{
	if (!pixels) return;

	float* data = reinterpret_cast<float*>(pixels);

	for (int y = 0; y < textureSize; y++)
	{
		for (int x = 0; x < textureSize; x++)
		{
			float* pixel = data + (y * textureSize + x) * 4;

			pixel[0] = sinf(x * 0.13f + time * 2.1f) * 0.5f + 0.5f; // R
			pixel[1] = sinf(y * 0.17f + time * 1.9f) * 0.5f + 0.5f; // G
			pixel[2] = cosf(x * 0.21f + time * 2.3f) * 0.5f + 0.5f; // B
			pixel[3] = 1.0f; // A
		}
	}
}