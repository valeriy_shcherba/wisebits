﻿using UnityEngine;

namespace Source.Components
{
	public class LookAt : MonoBehaviour
	{
		[SerializeField] private Transform _target;
		
		private void Update()
		{
			transform.LookAt(transform.position + _target.rotation * Vector3.forward,
				_target.rotation * Vector3.up);
		}
	}
}