﻿using UnityEngine;

namespace Source.Components
{
    public class RotateAround : MonoBehaviour
    {
        [SerializeField] private Transform _target;

        [SerializeField] private float _rotationSpeed = 1f;

        private void Update()
        {
            transform.RotateAround(_target.position, Vector3.up, _rotationSpeed);
        }
    }
}