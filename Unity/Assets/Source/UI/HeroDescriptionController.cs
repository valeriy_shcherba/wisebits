﻿using UnityEngine;

namespace Source.UI
{
    public class HeroDescriptionController : MonoBehaviour
    {
        [SerializeField] private HeroDescriptionView _view;

        public void SetHP(int hp)
        {
            _view.HPText.text = $"HP ({hp}/100)";
            _view.HPImage.fillAmount = hp / 100f;
        }
    }
}

