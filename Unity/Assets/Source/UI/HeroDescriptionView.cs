﻿using UnityEngine;
using UnityEngine.UI;

namespace Source.UI
{
	public class HeroDescriptionView : MonoBehaviour
	{
		[SerializeField] private Text _hpText;
		[SerializeField] private Image _hpImage;
		
		public Text HPText => _hpText;
		public Image HPImage => _hpImage;
	}
}