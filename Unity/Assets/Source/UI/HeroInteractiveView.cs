﻿using UnityEngine;
using UnityEngine.UI;

namespace Source.UI
{
	public class HeroInteractiveView : MonoBehaviour
	{
		[SerializeField] private Button _attackButton;
		[SerializeField] private Button _shoutButton;
		[SerializeField] private Button _jumpButton;
		[SerializeField] private Button _getHitButton;
		
		public Button AttackButton => _attackButton;
		public Button ShoutButton => _shoutButton;
		public Button JumpButton => _jumpButton;
		public Button GetHitButton => _getHitButton;
	}
}