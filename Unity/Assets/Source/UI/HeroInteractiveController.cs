﻿using Source.Controllers;
using UnityEngine;

namespace Source.UI
{
	[RequireComponent(typeof(HeroInteractiveView))]
	public class HeroInteractiveController : MonoBehaviour
	{
		[SerializeField] private HeroInteractiveView _view;
		[SerializeField] private HeroController _heroController;

		private void OnEnable()
		{
			_view.AttackButton.onClick.AddListener(OnAttackButtonClicked);
			_view.ShoutButton.onClick.AddListener(OnShoutButtonClicked);
			_view.JumpButton.onClick.AddListener(OnJumpButtonClicked);
			_view.GetHitButton.onClick.AddListener(OnGetHitButtonClicked);
		}

		private void OnDisable()
		{
			_view.AttackButton.onClick.RemoveAllListeners();
			_view.ShoutButton.onClick.RemoveAllListeners();
			_view.JumpButton.onClick.RemoveAllListeners();
			_view.GetHitButton.onClick.RemoveAllListeners();
		}

		private void OnAttackButtonClicked()
		{
			_heroController.StartAttackAnimation();
		}

		private void OnShoutButtonClicked()
		{
			_heroController.StartShoutAnimation();
		}

		private void OnJumpButtonClicked()
		{
			_heroController.StartJumpAnimation();
		}

		private void OnGetHitButtonClicked()
		{
			_heroController.StartGetHitAnimation();
		}
	}
}