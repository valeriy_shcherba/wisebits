﻿using System;
using System.Collections;
using MK.Glow;
using Source.UI;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

namespace Source.Controllers
{
	public class HeroController : MonoBehaviour
	{
		[SerializeField] private Animator _heroAnimator;

		[SerializeField] private MKGlowFree _postEffectGlowFree;
		[SerializeField] private BlurOptimized _postEffectBlur;

		[SerializeField] private HeroDescriptionController _descriptionController;
		
		private const string TRIGGER_ATTACK = "ATTACK";
		private const string TRIGGER_GETHIT = "GETHIT";
		private const string TRIGGER_JUMP = "JUMP";
		private const string TRIGGER_SHOUT = "SHOUT";
		private const string TRIGGER_RECOVERY = "RECOVERY";

		private const int POWER_ATTACK = 20;
		private const int POWER_SHOUT = 10;
		private const int POWER_JUMP = 5;
		private const int POWER_GETHIT = 1;
		
		private int _hp = 100;

		private bool _isBusy;
	
		private event Action _onAnimationEnded;
		private event Action _onAnimationStarted;

		private Coroutine _recoveryCoroutine;
		
		private void FixedUpdate()
		{
			if (!_postEffectGlowFree.enabled) return;
			
			_postEffectGlowFree.GlowTint = NativePlugin.GenerateAuraTexture().GetPixel(0,0);
		}
		
		public void StartAttackAnimation()
		{
			if (_isBusy || !CanStartAnimation(POWER_ATTACK)) return;

			_onAnimationStarted = () => DedactHP(POWER_ATTACK);
				
			StartAnimation(TRIGGER_ATTACK);
		}

		public void StartGetHitAnimation()
		{
			if (_isBusy || !CanStartAnimation(POWER_GETHIT)) return;

			_onAnimationStarted = () => DedactHP(POWER_GETHIT);
			
			StartAnimation(TRIGGER_GETHIT);
		}

		public void StartJumpAnimation()
		{
			if (_isBusy || !CanStartAnimation(POWER_JUMP)) return;
			
			_onAnimationStarted = () =>
			{
				DedactHP(POWER_JUMP);
				_postEffectBlur.enabled = true;
			};
			_onAnimationEnded = () => _postEffectBlur.enabled = false;
			
			StartAnimation(TRIGGER_JUMP);
		}

		public void StartShoutAnimation()
		{
			if (_isBusy || !CanStartAnimation(POWER_SHOUT)) return;
		
			_onAnimationStarted = () =>
			{
				DedactHP(POWER_SHOUT);
				_postEffectGlowFree.enabled = true;
			};	
			_onAnimationEnded = () => _postEffectGlowFree.enabled = false;
			
			StartAnimation(TRIGGER_SHOUT);
		}
		
		private void StartRecoveryAnimation()
		{
			if (_isBusy) return;

			_onAnimationStarted = () => _recoveryCoroutine = StartCoroutine(RecoveryHero());
			
			StartAnimation(TRIGGER_RECOVERY);
		}
		
		private void StartAnimation(string triggerName)
		{
			if (_recoveryCoroutine != null)
			{
				StopCoroutine(_recoveryCoroutine);
				_recoveryCoroutine = null;
			}
				
			_heroAnimator.SetTrigger(triggerName);
		}

		private void OnAnimationStarted()
		{
			_onAnimationStarted?.Invoke();
			_onAnimationStarted = null;
			
			_isBusy = true;
		}
		
		private void OnAnimationEnded()
		{
			_onAnimationEnded?.Invoke();
			_onAnimationEnded = null;
			
			_isBusy = false;

			if (_hp <= 0) StartRecoveryAnimation();
		}

		private bool CanStartAnimation(int power)
		{
			return _hp - power >= 0;
		}

		private void DedactHP(int power)
		{
			int result = _hp - power;
			_hp = result < 0 ? 0 : result;

			_descriptionController.SetHP(_hp);
		}

		private IEnumerator RecoveryHero()
		{
			float delay = 0.05f;

			while (_hp < 100)
			{
				_hp++;
				_descriptionController.SetHP(_hp);
				
				yield return new WaitForSeconds(delay);
			}

			_recoveryCoroutine = null;
		}
	}
}