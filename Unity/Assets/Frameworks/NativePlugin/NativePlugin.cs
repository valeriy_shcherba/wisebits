﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;


public class NativePlugin : MonoBehaviour
{
	private const int TEXTURE_SIZE = 32;
	
	[DllImport("NativePlugin")]
	private static extern void GenerateAuraTexture(IntPtr pixels, int size, int time);

	public static Texture2D GenerateAuraTexture()
	{
		Texture2D tex = new Texture2D(TEXTURE_SIZE, TEXTURE_SIZE, TextureFormat.ARGB32, false);
		
		Color[] pixels = tex.GetPixels (0);
		GCHandle handle = GCHandle.Alloc(pixels, GCHandleType.Pinned);

		GenerateAuraTexture(handle.AddrOfPinnedObject(), TEXTURE_SIZE, DateTime.Now.Second);

		tex.SetPixels (pixels, 0);
		tex.Apply ();

		return tex;
	}
}
